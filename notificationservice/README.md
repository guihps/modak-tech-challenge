# Notification Service

---

## Run the application

To start the application just run:
```shell
./gradlew run
```
This will also start all needed external resources (MongoDB)

## Send notification

To test sending a notification, you can use the endpoint: `POST /notifications`
Request body example:
```json
{"userId": "user", "type": "News", "message": "Good News!!"}
```
The available `type` values are: ` News, Update, Invite, Marketing`

cURL example:
```shell
curl --location --request POST 'http://localhost:8080/notifications' \
  --header 'Content-Type: application/json' \
  --data '{"userId": "user", "type": "News", "message": "Good News!!"}'
```

## Run tests

To run the project tests:
```shell
./gradlew test
```