package com.papacidro.notificationservice.core

data class Notification(val type: NotificationType, val userId: String, val message: String)

enum class NotificationType {
    News, Update, Invite, Marketing
}
