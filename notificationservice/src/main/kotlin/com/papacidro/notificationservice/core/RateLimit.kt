package com.papacidro.notificationservice.core

import kotlin.time.Duration

data class RateLimit(val count: Int, val period: String) {
    val duration = Duration.parse(period)
}