package com.papacidro.notificationservice.core

import com.papacidro.notificationservice.web.RateLimitExceededResponse

class RateLimitExceededException(
    val notification: Notification,
    val rateLimit: RateLimit
) : RuntimeException() {
    override val message = "Rate limit exceeded for this user and notification type"

    val errorResponse get() = RateLimitExceededResponse(
        message, notification.userId, notification.type, rateLimit.count, rateLimit.period.toString()
    )
}