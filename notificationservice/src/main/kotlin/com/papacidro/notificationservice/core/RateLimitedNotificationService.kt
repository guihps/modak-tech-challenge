package com.papacidro.notificationservice.core

import com.papacidro.notificationservice.core.interfaces.MessageGateway
import com.papacidro.notificationservice.core.interfaces.NotificationService
import com.papacidro.notificationservice.core.interfaces.SentNotificationRepository
import io.micronaut.context.annotation.Property
import jakarta.inject.Singleton
import org.slf4j.LoggerFactory

@Singleton
class RateLimitedNotificationService(
    private val messageGateway: MessageGateway,
    private val notificationRepository: SentNotificationRepository,
    @Property(name = "rate-limits") private val rateLimitsPerType: Map<String, RateLimit>
) : NotificationService {

    private val logger = LoggerFactory.getLogger(this::class.qualifiedName)

    override fun send(notification: Notification) {
        val rateLimit = rateLimitsPerType[notification.type.name.lowercase()]!!
        val sentCount = notificationRepository.getCountInPeriod(
            notification.userId,
            notification.type,
            rateLimit.duration.inWholeSeconds
        )

        if (sentCount < rateLimit.count) {
            messageGateway.send(notification.userId, notification.message)
            notificationRepository.insert(notification, rateLimit.duration.inWholeSeconds)
        } else {
            logger.info("Notification not sent, rate limit exceeded: $notification")
            throw RateLimitExceededException(notification, rateLimit)
        }
    }
}