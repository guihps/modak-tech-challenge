package com.papacidro.notificationservice.core.extensions

import java.time.LocalDateTime
import java.time.ZoneId

object LocalDateTimeExtension {
    fun utcNow() = LocalDateTime.now(ZoneId.of("UTC"))
}