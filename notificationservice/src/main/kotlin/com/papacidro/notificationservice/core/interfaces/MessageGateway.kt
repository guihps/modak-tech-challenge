package com.papacidro.notificationservice.core.interfaces

interface MessageGateway {
    fun send(userId: String, message: String)
}