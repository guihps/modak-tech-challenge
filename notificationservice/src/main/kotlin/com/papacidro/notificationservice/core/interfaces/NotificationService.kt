package com.papacidro.notificationservice.core.interfaces

import com.papacidro.notificationservice.core.Notification

interface NotificationService {
    fun send(notification: Notification)
}