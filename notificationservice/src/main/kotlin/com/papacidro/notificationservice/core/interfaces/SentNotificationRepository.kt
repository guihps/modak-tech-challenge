package com.papacidro.notificationservice.core.interfaces

import com.papacidro.notificationservice.core.Notification
import com.papacidro.notificationservice.core.NotificationType

interface SentNotificationRepository {
    fun getCountInPeriod(userId: String, type: NotificationType, period: Long) : Long
    fun insert(notification: Notification, expirationPeriod: Long)
}