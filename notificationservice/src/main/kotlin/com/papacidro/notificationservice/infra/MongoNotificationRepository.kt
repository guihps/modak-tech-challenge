package com.papacidro.notificationservice.infra

import com.mongodb.client.MongoClient
import com.mongodb.client.model.Filters
import com.mongodb.client.model.IndexOptions
import com.mongodb.client.model.Indexes
import com.papacidro.notificationservice.core.Notification
import com.papacidro.notificationservice.core.NotificationType
import com.papacidro.notificationservice.core.extensions.LocalDateTimeExtension
import com.papacidro.notificationservice.core.interfaces.SentNotificationRepository
import io.micronaut.context.annotation.Property
import jakarta.inject.Singleton
import java.util.concurrent.TimeUnit

@Singleton
class MongoNotificationRepository(
    mongoClient: MongoClient,
    @Property(name = "mongodb.database-name") private val databaseName: String
) : SentNotificationRepository {

    private val collection =
        mongoClient.getDatabase(databaseName).getCollection(SentNotification::class.simpleName!!)
            .withDocumentClass(SentNotification::class.java);

    init {
        collection.createIndex(Indexes.ascending(SentNotification::userId.name, SentNotification::type.name))
        collection.createIndex(
            Indexes.ascending(SentNotification::expireAt.name),
            IndexOptions().expireAfter(0L, TimeUnit.SECONDS)
        )
    }

    override fun getCountInPeriod(userId: String, type: NotificationType, period: Long): Long {
        val filter = Filters.and(
            Filters.eq(SentNotification::userId.name, userId),
            Filters.eq(SentNotification::type.name, type.name),
            Filters.gte(SentNotification::createdAt.name, LocalDateTimeExtension.utcNow().minusSeconds(period))
        )
        return collection.countDocuments(filter)
    }

    override fun insert(notification: Notification, expirationPeriod: Long) {
        collection.insertOne(SentNotification(notification, expirationPeriod))
    }
}