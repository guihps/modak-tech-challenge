package com.papacidro.notificationservice.infra

import com.papacidro.notificationservice.core.Notification
import com.papacidro.notificationservice.core.NotificationType
import com.papacidro.notificationservice.core.extensions.LocalDateTimeExtension
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId
import java.time.LocalDateTime


data class SentNotification(
    var userId: String = "",
    var type: NotificationType = NotificationType.News,
    var createdAt: LocalDateTime = LocalDateTime.now(),
    var expireAt: LocalDateTime = LocalDateTime.now(),
    @BsonId var id: ObjectId? = null
) {
    constructor(notification: Notification, expirationPeriod: Long) :
            this(
                notification.userId,
                notification.type,
                LocalDateTimeExtension.utcNow(),
                LocalDateTimeExtension.utcNow().plusSeconds(expirationPeriod)
            )
}