package com.papacidro.notificationservice.infra

import com.papacidro.notificationservice.core.interfaces.MessageGateway
import jakarta.inject.Singleton
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Singleton
class SimpleMessageGateway : MessageGateway {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    override fun send(userId: String, message: String) {
        logger.info("sending message to user $userId: $message")
    }
}