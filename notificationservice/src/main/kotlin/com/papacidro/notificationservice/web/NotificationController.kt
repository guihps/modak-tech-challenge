package com.papacidro.notificationservice.web

import com.papacidro.notificationservice.core.Notification
import com.papacidro.notificationservice.core.RateLimitExceededException
import com.papacidro.notificationservice.core.interfaces.NotificationService
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post

@Controller("/notifications")
class NotificationController(private val notificationService: NotificationService) {

    @Post
    fun sendNotification(@Body notification: Notification): HttpResponse<Any> {
        try{
            notificationService.send(notification)
            return HttpResponse.created(notification)
        } catch (ex: RateLimitExceededException) {
            return HttpResponse.badRequest(ex.errorResponse)
        }
    }
}