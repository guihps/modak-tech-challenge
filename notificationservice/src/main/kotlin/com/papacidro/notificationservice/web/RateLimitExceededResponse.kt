package com.papacidro.notificationservice.web

import com.papacidro.notificationservice.core.NotificationType

data class RateLimitExceededResponse(
    val message: String,
    val userId: String,
    val notificationType: NotificationType,
    val rate: Int,
    val period: String
)