package com.papacidro.notificationservice

import com.appmattus.kotlinfixture.kotlinFixture
import com.papacidro.notificationservice.core.Notification
import com.papacidro.notificationservice.core.NotificationType
import com.papacidro.notificationservice.web.RateLimitExceededResponse
import io.kotest.assertions.nondeterministic.eventually
import io.kotest.assertions.nondeterministic.eventuallyConfig
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.be
import io.kotest.matchers.should
import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest.POST
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.test.extensions.kotest5.annotation.MicronautTest
import kotlin.time.Duration.Companion.seconds

@MicronautTest(environments = ["test"])
class NotificationServiceAppTest(
    @Client("/notifications") private val client: HttpClient
) : FreeSpec({
    val fixture = kotlinFixture()
    val userId = fixture<String>()

    "The Notification Service should" - {
        "successfully send notifications while rate limit is not reached" {
            val messages = (1..5).map { fixture<String>() }
            messages.map { m ->
                val notification = Notification(NotificationType.News, userId, m)

                val response = client.toBlocking().exchange(POST("/", notification), Notification::class.java)

                response.status should be(HttpStatus.CREATED)
                response.body().userId should be(userId)
                response.body().type should be(NotificationType.News)
                response.body().message should be(m)
            }
        }

        "block notification after rate limit is exceeded" {
            val messages = (1..2).map { fixture<String>() }
            messages.map { m ->
                val notification = Notification(NotificationType.News, userId, m)

                val exception = shouldThrow<HttpClientResponseException> {
                    client.toBlocking().exchange(
                        POST("/", notification),
                        Argument.of(Notification::class.java),
                        Argument.of(RateLimitExceededResponse::class.java)
                    )
                }

                val responseBody = exception.response.getBody(RateLimitExceededResponse::class.java).get()
                exception.status should be(HttpStatus.BAD_REQUEST)
                responseBody.rate should be(5)
                responseBody.period should be("5s")
            }
        }
    }

    "Meanwhile, it should" - {
        "keep allowing notifications with another types for same user" {
            val messages = listOf(NotificationType.Update, NotificationType.Invite).map { it to fixture<String>() }
            messages.map { (t, m) ->
                val notification = Notification(t, userId, m)

                val response = client.toBlocking().exchange(POST("/", notification), Notification::class.java)

                response.status should be(HttpStatus.CREATED)
                response.body().userId should be(userId)
                response.body().type should be(t)
                response.body().message should be(m)
            }
        }

        "keep allowing notifications for another users" {
            val messages = (1..2).map { fixture<String>() to fixture<String>() }
            messages.map { (u, m) ->
                val notification = Notification(NotificationType.News, u, m)

                val response = client.toBlocking().exchange(POST("/", notification), Notification::class.java)

                response.status should be(HttpStatus.CREATED)
                response.body().userId should be(u)
                response.body().type should be(NotificationType.News)
                response.body().message should be(m)
            }
        }
    }

    "After the waiting time, it should" - {
        "allow that type of notification for the same user again" {
            val message = fixture<String>()
            val notification = Notification(NotificationType.News, userId, message)

            val config = eventuallyConfig {
                duration = 6.seconds
                initialDelay = 5.seconds
                retries = 1
            }

            eventually(config) {
                val response = client.toBlocking().exchange(POST("/", notification), Notification::class.java)

                response.status should be(HttpStatus.CREATED)
                response.body().userId should be(userId)
                response.body().type should be(NotificationType.News)
                response.body().message should be(message)
            }
        }
    }
})