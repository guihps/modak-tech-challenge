package com.papacidro.notificationservice.core

import com.papacidro.notificationservice.core.interfaces.MessageGateway
import com.papacidro.notificationservice.core.interfaces.SentNotificationRepository
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.IsolationMode
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.core.spec.style.Test
import io.kotest.matchers.be
import io.kotest.matchers.should
import io.mockk.*

@Test
class RateLimitedNotificationServiceTest : ShouldSpec({
    isolationMode = IsolationMode.InstancePerTest
    val userId = "user-id"
    val message = "message"
    val messageGatewayMock: MessageGateway = mockk()
    val notificationRepositoryMock: SentNotificationRepository = mockk()
    val rateLimitsPerType = mapOf(
        NotificationType.News.name.lowercase() to RateLimit(1, "1s"),
        NotificationType.Update.name.lowercase() to RateLimit(10, "1m"),
    )
    val service = RateLimitedNotificationService(messageGatewayMock, notificationRepositoryMock, rateLimitsPerType)

    context("send()") {
        should("send notification when rate limit is not exceeded") {
            every { messageGatewayMock.send(any(), any()) } just runs
            every { notificationRepositoryMock.getCountInPeriod(userId, NotificationType.Update, 60) } returns 2
            every { notificationRepositoryMock.insert(any(), any()) } just runs

            val notification = Notification(NotificationType.Update, userId, message)
            service.send(notification)

            verify { messageGatewayMock.send(userId, message) }
            verify { notificationRepositoryMock.insert(notification, 60) }
        }
        should("raise exception when rate limit is exceeded") {
            every { messageGatewayMock.send(any(), any()) } just runs
            every { notificationRepositoryMock.getCountInPeriod(userId, NotificationType.News, 1) } returns 2
            every { notificationRepositoryMock.insert(any(), any()) } just runs

            val exception = shouldThrow<RateLimitExceededException> {
                service.send(Notification(NotificationType.News, userId, message))
            }

            exception.rateLimit.count should be(1)
            exception.rateLimit.period should be("1s")
            exception.notification.type should be(NotificationType.News)
            exception.notification.userId should be(userId)

            verify(exactly = 0) { messageGatewayMock.send(any(), any())}
            verify(exactly = 0) { notificationRepositoryMock.insert(any(), any()) }
        }
    }
})