package com.papacidro.notificationservice.infra

import com.appmattus.kotlinfixture.kotlinFixture
import com.mongodb.client.MongoClient
import com.mongodb.client.model.Filters
import com.papacidro.notificationservice.core.Notification
import com.papacidro.notificationservice.core.NotificationType
import com.papacidro.notificationservice.core.extensions.LocalDateTimeExtension
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.be
import io.kotest.matchers.should
import io.micronaut.context.annotation.Property
import io.micronaut.test.extensions.kotest5.annotation.MicronautTest
import java.time.temporal.ChronoUnit
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours

@MicronautTest
class MongoNotificationRepositoryTest(
    private val mongoClient: MongoClient,
    @Property(name = "mongodb.database-name") val databaseName: String
) : ShouldSpec({
    val fixture = kotlinFixture()
    val repository = MongoNotificationRepository(mongoClient, databaseName)
    val collection = mongoClient.getDatabase(databaseName)
        .getCollection(SentNotification::class.simpleName!!, SentNotification::class.java)

    fun dateInRange(period: Long) = LocalDateTimeExtension.utcNow()
        .minusSeconds(fixture<Long>(1..(period - 10)))

    fun dateOutOfRange(period: Long) = LocalDateTimeExtension.utcNow()
        .minusSeconds(fixture<Long>(period..1.days.inWholeSeconds))

    fun futureDate() = LocalDateTimeExtension.utcNow().plusMinutes(30)

    beforeEach { collection.deleteMany(Filters.empty()) }

    should("create indexes on start") {
        collection.listIndexes().count() should be(3)
    }

    context("getCountInPeriod()") {
        should("get sent notifications count for specified user and type in the valid period") {
            val period = 1.hours.inWholeSeconds
            val sentNotifications = listOf(
                SentNotification("user-1", NotificationType.News, dateOutOfRange(period), futureDate()),
                SentNotification("user-1", NotificationType.News, dateInRange(period), futureDate()),
                SentNotification("user-1", NotificationType.News, dateInRange(period), futureDate()),
                SentNotification("user-2", NotificationType.News, dateInRange(period), futureDate()),
                SentNotification("user-1", NotificationType.Update, dateInRange(period), futureDate()),
                SentNotification("user-1", NotificationType.Update, dateOutOfRange(period), futureDate()),
                SentNotification("user-2", NotificationType.News, dateOutOfRange(period), futureDate()),
                SentNotification("user-2", NotificationType.Update, dateOutOfRange(period), futureDate())
            )

            collection.insertMany(sentNotifications)

            val count = repository.getCountInPeriod("user-1", NotificationType.News, period)

            count should be(2)
        }
    }

    context("insert()") {
        should("insert new document with correct values") {
            val notification = Notification(NotificationType.Update, "userId", "message")
            val expirationPeriod = fixture<Long>(10..1.days.inWholeSeconds)

            repository.insert(notification, expirationPeriod)

            val documents = collection.find()
            documents.count() should be(1)
            val sentNotification = documents.first()!!
            sentNotification.userId should be(notification.userId)
            sentNotification.type should be(notification.type)
            val savedExpiration = ChronoUnit.SECONDS.between(sentNotification.createdAt, sentNotification.expireAt)
            savedExpiration should be(expirationPeriod)
        }
    }
})