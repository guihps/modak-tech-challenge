package com.papacidro.notificationservice.web

import com.appmattus.kotlinfixture.kotlinFixture
import com.papacidro.notificationservice.core.Notification
import com.papacidro.notificationservice.core.RateLimit
import com.papacidro.notificationservice.core.RateLimitExceededException
import com.papacidro.notificationservice.core.interfaces.NotificationService
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.be
import io.kotest.matchers.should
import io.micronaut.http.HttpStatus
import io.micronaut.test.extensions.kotest5.annotation.MicronautTest
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import kotlin.time.Duration

@MicronautTest
class NotificationControllerTest: ShouldSpec({
    val fixture = kotlinFixture()
    val notificationService: NotificationService = mockk()
    val controller = NotificationController(notificationService)

    context("sendNotification") {
        should("return Created and notification data") {
            val notification = fixture<Notification>()
            every { notificationService.send(any()) } just runs

            val response = controller.sendNotification(notification)

            response.status() should be(HttpStatus.CREATED)
            (response.body() as Notification) should be(notification)
        }
        should("return BadRequest and error data when rate limit is exceeded"){
            val notification = fixture<Notification>()
            val rateLimit = RateLimit(fixture<Int>(), fixture<Duration>().toString())
            every { notificationService.send(any()) } throws RateLimitExceededException(notification, rateLimit)

            val response = controller.sendNotification(notification)

            response.status() should be(HttpStatus.BAD_REQUEST)
            val body = response.body() as RateLimitExceededResponse
            body.userId should be(notification.userId)
            body.notificationType should be(notification.type)
            body.rate should be(rateLimit.count)
            body.period should be(rateLimit.period)
        }
    }
})